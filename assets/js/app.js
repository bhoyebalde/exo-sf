/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');
import 'bootstrap';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import 'bootstrap/dist/css/bootstrap.min.css';

const $ = require('jquery');

const routes = require('../../public/js/fos_js_routes.json');

import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';

Routing.setRoutingData(routes);
global.Routing = Routing;

// require('jquery-ui');
require('jquery-ui/ui/widgets/autocomplete');
global.$ = global.jQuery = $;


$( "#ville" )
    .autocomplete({
        source: function( request, response ) {
            $.getJSON( "https://geo.api.gouv.fr/communes", {
                nom: request.term
            }, response );
        },
        search: function() {
            var term = this.value ;
            if ( term.length < 2 ) {
                return false;
            }
        }, 
        minLength : 2,
        select: function( event, ui ) {
            $('#ville').val(ui.item.nom);

            $.ajax({
            url: "https://api-adresse.data.gouv.fr/search/?q="+ui.item.nom,
                context: document.body
            }).done(function(e) {

                if (e.features.length > 0) {
                    var coordonnees = e.features[0].geometry.coordinates;

                    document.location = Routing.generate('weather', { lat: coordonnees[1],lon: coordonnees[0],ville : ui.item.nom});
                }
            });

            return false;
        }
    })
     .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append( "<div>" + item.nom + "</div>" )
            .appendTo( ul );
    }; 

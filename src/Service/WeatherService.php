<?php

namespace App\Service;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
class WeatherService
{
    private $client;
    private $apiKey;

    public function __construct($apiKey)
    {
        $this->client = HttpClient::create();
        $this->apiKey = $apiKey;
    }

    /**
     * @return array
     */
    public function getWeather($lat,$lon)
    {
        $response = $this->client->request('GET', 'https://api.darksky.net/forecast/' . $this->apiKey . '/'.$lat.','.$lon.'?lang=fr&units=auto');
        $response = json_decode($response->getContent(),true);

        return $response;
    }

    public function timestampToDate($timestamp) {
        return date('\L\e Y-m-d',$timestamp);
    }
}
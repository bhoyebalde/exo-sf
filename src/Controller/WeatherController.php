<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\WeatherService;

class WeatherController extends AbstractController
{
    private $wheatherService;

    public function __construct(WeatherService $weather)
    {
        $this->weatherService = $weather;
    }

    /**
     * @Route("/{lat}/{lon}/{ville}", name="weather", options={"expose"=true})
     */
    public function index($lat="",$lon = "",$ville = "")
    {
        $weather = null;

        if ($lat != "" && $lon != "")
            $weather = $this->weatherService->getWeather($lat,$lon);

        return $this->render('weather/index.html.twig', array(
           'weather' => $weather,
           'ville' => $ville 
        ));
    }
}